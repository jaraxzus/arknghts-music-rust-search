FROM rust:1.75 as builder

WORKDIR /app

COPY . .

RUN cargo build --release

# Production stage
FROM debian:stable-slim
WORKDIR /usr/local/bin
COPY --from=builder /app/target/release/arknights_music_search .
EXPOSE 8080
CMD ["./arknights_music_search"]



