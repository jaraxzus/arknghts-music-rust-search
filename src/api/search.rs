use actix_web::web;
use actix_web::{get, HttpResponse, Responder};
use serde_json::json;

use crate::schemas::schemas::SearchValue;
use crate::services::search as SearchService;
use crate::state::GuardedAppState;

#[get("/api/rust/search/")]
pub async fn search(
    opts: web::Query<SearchValue>,
    data: web::Data<GuardedAppState>,
) -> impl Responder {
    let result = SearchService::search(&opts.value, &data).await;

    println!("GET /api/rust/seach/ OK!");
    if let Ok(result) = result {
        HttpResponse::Ok().json(json!(result))
    } else {
        println!("{result:#?}");
        HttpResponse::NotFound().json(json!("Not Found"))
    }
}
