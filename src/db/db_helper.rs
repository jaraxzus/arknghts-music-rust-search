use anyhow::Result;
use dotenv::dotenv;
use sqlx::{postgres::PgListener, PgPool};
use std::{env, sync::Arc};
use tokio::sync::RwLock;

use crate::{schemas::schemas::Album, services::update::update_state_albums};

#[derive(Debug, Clone)]
pub struct DBHelper {
    pub pool: PgPool,
}

impl DBHelper {
    pub async fn new() -> Self {
        let pool = PgPool::connect(&get_database_url())
            .await
            .expect("Failed to create database pool");

        Self { pool }
    }
    pub async fn check_triger(&self) -> Result<()> {
        // Проверяем наличие функции
        let function_exists: bool = sqlx::query_scalar(
            "SELECT EXISTS (
                SELECT 1 FROM pg_proc WHERE proname = 'notify_changes'
                )",
        )
        .fetch_one(&self.pool)
        .await?;

        // Если функция не существует, создаем ее
        if !function_exists {
            sqlx::query(
                r#"
            CREATE OR REPLACE FUNCTION notify_changes()
            RETURNS TRIGGER AS $$
            DECLARE
                table_name TEXT;
                message TEXT;
            BEGIN
                -- Получаем имя таблицы, где произошли изменения
                table_name := TG_TABLE_NAME;

                -- Создаем сообщение о событии
                IF TG_OP = 'DELETE' THEN
                    message := 'Deleted from ' || table_name;
                ELSIF TG_OP = 'INSERT' THEN
                    message := 'Inserted into ' || table_name;
                ELSIF TG_OP = 'UPDATE' THEN
                    message := 'Updated in ' || table_name;
                ELSE
                    message := 'Unknown operation in ' || table_name;
                END IF;

                -- Отправляем уведомление на канал "changes_channel"
                PERFORM pg_notify('changes_channel', message);

                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
            "#,
            )
            .execute(&self.pool)
            .await?;
        }

        let tables = &[
            "album_artist_association",
            "albums",
            "artists",
            "song_artist_association",
            "songs",
            "tag_album_association",
            "tag_artist_association",
            "tag_song_association",
            "tags",
        ];
        for table in tables {
            let trigger_name = format!("{}_changes_trigger", table);
            create_trigger(&self.pool, &trigger_name, table)
                .await
                .expect("Failed to create trigger");
        }
        Ok(())
    }
}

async fn create_trigger(pool: &PgPool, trigger_name: &str, table: &str) -> Result<(), sqlx::Error> {
    // Проверяем наличие триггера
    let trigger_exists: bool = sqlx::query_scalar(
        format!(
            "SELECT EXISTS (SELECT 1 FROM pg_trigger WHERE tgname = '{}')",
            trigger_name
        )
        .as_str(),
    )
    .fetch_one(pool)
    .await?;

    // Если триггер не существует, создаем его
    if !trigger_exists {
        let query = format!(
            r#"
                CREATE TRIGGER {name}
                AFTER INSERT OR UPDATE OR DELETE
                ON {table}
                FOR EACH STATEMENT
                EXECUTE FUNCTION notify_changes();
            "#,
            name = trigger_name,
            table = table
        );

        sqlx::query(&query).execute(pool).await?;
    }
    Ok(())
}

pub async fn change_listener<'a>(
    pool: &'a PgPool,
    state_albums: Arc<RwLock<Vec<Album>>>,
) -> Result<()> {
    let mut listener = PgListener::connect_with(&pool).await.unwrap();
    // , "changes_channel"
    listener.listen("changes_channel").await?;

    // Обрабатываем уведомления
    while let Ok(_) = listener.recv().await {
        // println!("Received notification: {:?}", notification.payload());
        update_state_albums(&pool, state_albums.clone()).await?;
    }
    Ok(())
}

fn load_env() {
    dotenv().ok();
}

fn get_database_url() -> String {
    load_env();
    let url = env::var("DATABASE_URL").expect("set postgres url env");
    url

    // let user =
    //     env::var("POSTGRES_USER").expect("Please set the POSTGRES_USER environment variable");
    // let password = env::var("POSTGRES_PASSWORD")
    //     .expect("Please set the POSTGRES_PASSWORD environment variable");
    // let database = env::var("POSTGRES_DATABASE")
    //     .expect("Please set the POSTGRES_DATABASE environment variable");
    // let host =
    //     env::var("POSTGRES_HOST").expect("Please set the POSTGRES_HOST environment variable");
    // let port =
    //     env::var("POSTGRES_PORT").expect("Please set the POSTGRES_PORT environment variable");
    //
    // format!(
    //     "postgres://{}:{}@{}:{}/{}",
    //     user, password, host, port, database
    // )
}
