#[derive(Debug, sqlx::FromRow)]
pub struct Tag {
    pub cid: i32,
    pub tag: String,
}
#[derive(Debug, sqlx::FromRow)]
pub struct Artist {
    pub cid: i32,
    pub name: String,
}
#[derive(Debug, sqlx::FromRow)]
pub struct RowAlbum {
    pub name: String,
    pub cover_url: String,
    pub intro: String,
    pub belong: String,
    pub cover_de_url: String,
    pub cid: i32,
}

#[derive(Debug, sqlx::FromRow)]
pub struct RowSong {
    pub name: String,
    pub album_cid: i32,
    pub source_url: String,
    pub lyric_url: Option<String>,
    pub mv_url: Option<String>,
    pub mv_cover_url: Option<String>,
    pub cid: i32,
}
#[derive(Debug, sqlx::FromRow)]
pub struct AlbumArtist {
    pub cid: i32,
    pub album_cid: i32,
    pub artist_cid: i32,
}
#[derive(Debug, sqlx::FromRow)]
pub struct AlbumTag {
    pub cid: i32,
    pub album_cid: i32,
    pub tag_cid: i32,
}
#[derive(Debug, sqlx::FromRow)]
pub struct SongArtist {
    pub cid: i32,
    pub song_cid: i32,
    pub artist_cid: i32,
}
#[derive(Debug, sqlx::FromRow)]
pub struct SongTag {
    pub cid: i32,
    pub song_cid: i32,
    pub tag_cid: i32,
}
