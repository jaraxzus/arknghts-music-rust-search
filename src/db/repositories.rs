use anyhow::Result;

use super::models::{AlbumArtist, AlbumTag, Artist, RowAlbum, RowSong, SongArtist, SongTag, Tag};

pub trait Repositori {
    type Model;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>>;
}

pub struct AlbumRepositori;
impl Repositori for AlbumRepositori {
    type Model = RowAlbum;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let albums = sqlx::query_as!(Self::Model, "select * from albums",)
            .fetch_all(conn)
            .await?;
        Ok(albums)
    }
}
pub struct SongRepositori;
impl Repositori for SongRepositori {
    type Model = RowSong;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let songs: Vec<RowSong> = sqlx::query_as!(Self::Model, "select * FROM songs")
            .fetch_all(conn)
            .await?;
        Ok(songs)
    }
}

pub struct TagRepositiry;
impl Repositori for TagRepositiry {
    type Model = Tag;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let tags = sqlx::query_as!(Self::Model, "select * from tags",)
            .fetch_all(conn)
            .await?;
        Ok(tags)
    }
}

pub struct ArtistRepositori;
impl Repositori for ArtistRepositori {
    type Model = Artist;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let artists = sqlx::query_as!(Self::Model, "select * from artists")
            .fetch_all(conn)
            .await?;
        Ok(artists)
    }
}
pub struct AlbumArtistRepositori;
impl Repositori for AlbumArtistRepositori {
    type Model = AlbumArtist;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let artists = sqlx::query_as!(Self::Model, "select * from album_artist_association")
            .fetch_all(conn)
            .await?;
        Ok(artists)
    }
}
pub struct AlbumTagRepositori;
impl Repositori for AlbumTagRepositori {
    type Model = AlbumTag;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let artists = sqlx::query_as!(Self::Model, "select * from tag_album_association")
            .fetch_all(conn)
            .await?;
        Ok(artists)
    }
}

pub struct SongArtistRepositori;
impl Repositori for SongArtistRepositori {
    type Model = SongArtist;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let artists = sqlx::query_as!(Self::Model, "select * from song_artist_association")
            .fetch_all(conn)
            .await?;
        Ok(artists)
    }
}
pub struct SongTagRepositori;
impl Repositori for SongTagRepositori {
    type Model = SongTag;
    async fn get_all(conn: &sqlx::PgPool) -> Result<Vec<Self::Model>> {
        let artists = sqlx::query_as!(Self::Model, "select * from tag_song_association")
            .fetch_all(conn)
            .await?;
        Ok(artists)
    }
}
