mod api;
mod db;
mod schemas;
mod services;
mod state;

use actix_cors::Cors;
use actix_web::{http, web, App, HttpServer};
use api::search::search;
use db::db_helper::{change_listener, DBHelper};
use services::update::update_state_albums;
use state::{AppState, GuardedAppState};
use std::sync::Arc;
use tokio::sync::RwLock;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Starting job");
    let db = DBHelper::new().await;
    let albums = Arc::new(RwLock::new(vec![]));
    db.check_triger().await.unwrap();
    update_state_albums(&db.pool, albums.clone()).await.unwrap();
    let pool_for_listener = db.clone().pool;
    let albums_for_listener = albums.clone();
    tokio::spawn(async move {
        change_listener(&pool_for_listener, albums_for_listener)
            .await
            .unwrap()
    });
    let state = web::Data::new(GuardedAppState {
        state: AppState { db, albums },
    });
    HttpServer::new(move || {
        let cors = Cors::default()
            .allowed_origin("http://localhost")
            .allowed_origin("http://localhost:8080")
            .allowed_origin("http://localhost:3000")
            .allowed_headers(vec![
                http::header::ACCESS_CONTROL_ALLOW_HEADERS,
                http::header::ACCESS_CONTROL_ALLOW_CREDENTIALS,
                http::header::ACCESS_CONTROL_ALLOW_ORIGIN,
            ])
            .supports_credentials()
            .allowed_methods(vec!["GET"]);
        App::new()
            .wrap(cors)
            .app_data(state.clone())
            .service(search)
    })
    .bind(("0.0.0.0", 8090))?
    .run()
    .await
}
