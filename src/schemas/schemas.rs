use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct SearchValue {
    pub value: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct Tag {
    pub cid: i32,
    pub tag: String,
}
#[derive(Debug, Serialize, Clone)]
pub struct Artist {
    pub cid: i32,
    pub name: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct Song {
    pub name: String,
    pub album_cid: i32,
    pub source_url: String,
    pub lyric_url: Option<String>,
    pub mv_url: Option<String>,
    pub mv_cover_url: Option<String>,
    pub cid: i32,
    pub artists: Option<Vec<Artist>>,
    pub tags: Option<Vec<Tag>>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Album {
    pub name: String,
    pub cover_url: String,
    pub intro: String,
    pub belong: String,
    pub cover_de_url: String,
    pub cid: i32,
    pub songs: Option<Vec<Song>>,
    pub artists: Option<Vec<Artist>>,
    pub tags: Option<Vec<Tag>>,
}

#[derive(Default, Debug, Serialize)]
pub struct Albums {
    pub albums: Vec<Album>,
}
