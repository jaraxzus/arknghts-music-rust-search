use actix_web::web;
use anyhow::Result;

use crate::{
    schemas::schemas::{Album, Song},
    state::GuardedAppState,
};

pub fn string_process(input: &str, target: &str) -> bool {
    input
        .trim_start()
        .to_lowercase()
        .contains(&target.trim_start().to_lowercase())
}

fn check_album(album: &Album, target: &str) -> bool {
    let fields_to_check = vec![&album.name, &album.intro, &album.belong];

    for field in fields_to_check {
        if string_process(field, target) {
            return true;
        }
    }
    if let Some(tags) = &album.tags {
        for tag in tags {
            if string_process(&tag.tag, target) {
                return true;
            }
        }
    }
    if let Some(artists) = &album.artists {
        for artist in artists {
            if string_process(&artist.name, target) {
                return true;
            }
        }
    }
    false
}
pub fn check_song(target: &str, song: &Song) -> bool {
    let fields_to_check = vec![&song.name];

    for field in fields_to_check {
        if string_process(field, target) {
            return true;
        }
    }
    if let Some(tags) = &song.tags {
        for tag in tags {
            if string_process(&tag.tag, target) {
                return true;
            }
        }
    }
    if let Some(artists) = &song.artists {
        for artist in artists {
            if string_process(&artist.name, target) {
                return true;
            }
        }
    }

    false
}
pub async fn search(value: &str, data: &web::Data<GuardedAppState>) -> Result<Vec<Album>> {
    let albums = data.state.albums.read().await.clone();

    let mut result = vec![];
    for mut album in albums {
        if check_album(&album, value) {
            result.push(album)
        } else {
            if let Some(songs) = album.songs {
                let filtered: Vec<Song> = songs
                    .into_iter()
                    .filter(|song| check_song(value, song))
                    .collect();
                if filtered.len() > 0 {
                    album.songs = Some(filtered);
                    result.push(album)
                }
            };
        }
    }
    Ok(result)
}
