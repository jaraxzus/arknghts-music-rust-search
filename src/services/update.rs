use std::sync::Arc;

use crate::db::models::{
    AlbumArtist, AlbumTag, Artist as MArtist, RowAlbum, RowSong, SongArtist, SongTag, Tag as MTag,
};
use crate::db::repositories::{
    AlbumArtistRepositori, AlbumRepositori, AlbumTagRepositori, ArtistRepositori, Repositori,
    SongArtistRepositori, SongRepositori, SongTagRepositori, TagRepositiry,
};
use crate::schemas::schemas::{Album, Artist, Song, Tag};
use anyhow::Result;
use futures::try_join;
use tokio::sync::RwLock;

pub async fn update_state_albums(
    conn: &sqlx::PgPool,
    state_albums: Arc<RwLock<Vec<Album>>>,
) -> Result<()> {
    let albums = AlbumRepositori::get_all(&conn);
    let songs = SongRepositori::get_all(&conn);
    let artists = ArtistRepositori::get_all(&conn);
    let tags = TagRepositiry::get_all(&conn);
    let album_artists = AlbumArtistRepositori::get_all(&conn);
    let album_tags = AlbumTagRepositori::get_all(&conn);
    let song_artists = SongArtistRepositori::get_all(&conn);
    let song_tags = SongTagRepositori::get_all(&conn);

    let (albums, songs, artists, tags, album_artists, album_tags, song_artists, song_tags) = try_join!(
        albums,
        songs,
        artists,
        tags,
        album_artists,
        album_tags,
        song_artists,
        song_tags
    )?;
    let full_songs = songs
        .into_iter()
        .map(|row_song| build_song(row_song, &tags, &song_tags, &artists, &song_artists))
        .collect();

    let mut sa = state_albums.write().await;
    sa.clear();
    albums.into_iter().for_each(|row_album| {
        sa.push(build_album(
            row_album,
            &full_songs,
            &tags,
            &album_tags,
            &artists,
            &album_artists,
        ))
    });
    Ok(())
}
pub fn build_song(
    row_song: RowSong,
    tags: &Vec<MTag>,
    song_tag: &Vec<SongTag>,
    artists: &Vec<MArtist>,
    song_artists: &Vec<SongArtist>,
) -> Song {
    let mut song = Song {
        name: row_song.name,
        album_cid: row_song.album_cid,
        source_url: row_song.source_url,
        lyric_url: row_song.lyric_url,
        mv_url: row_song.mv_url,
        mv_cover_url: row_song.mv_cover_url,
        cid: row_song.cid,
        artists: Some(vec![]),
        tags: Some(vec![]),
    };
    let relation_tags: Vec<&SongTag> = song_tag
        .iter()
        .filter(|relation| relation.song_cid == song.cid)
        .collect();
    if relation_tags.len() > 0 {
        let db_tags: Vec<&MTag> = tags
            .iter()
            .filter(|tag| {
                let relation = relation_tags
                    .iter()
                    .find(|relation| tag.cid == relation.tag_cid);
                if let Some(_) = relation {
                    return true;
                } else {
                    return false;
                }
            })
            .collect();
        song.tags = Some(
            db_tags
                .iter()
                .map(|db_tag| Tag {
                    cid: db_tag.cid,
                    tag: db_tag.tag.clone(),
                })
                .collect(),
        )
    }
    let relation_artists: Vec<&SongArtist> = song_artists
        .iter()
        .filter(|relation| relation.song_cid == song.cid)
        .collect();
    if relation_artists.len() > 0 {
        let db_tags: Vec<&MArtist> = artists
            .iter()
            .filter(|artist| {
                let relation = relation_artists
                    .iter()
                    .find(|relation| artist.cid == relation.artist_cid);
                if let Some(_) = relation {
                    return true;
                } else {
                    return false;
                }
            })
            .collect();
        song.artists = Some(
            db_tags
                .iter()
                .map(|db_artist| Artist {
                    cid: db_artist.cid,
                    name: db_artist.name.clone(),
                })
                .collect(),
        )
    }
    song
}
pub fn build_album(
    row_album: RowAlbum,
    songs: &Vec<Song>,
    tags: &Vec<MTag>,
    album_tag: &Vec<AlbumTag>,
    artists: &Vec<MArtist>,
    album_artists: &Vec<AlbumArtist>,
) -> Album {
    let mut album = Album {
        name: row_album.name,
        cover_url: row_album.cover_url,
        intro: row_album.intro,
        belong: row_album.belong,
        cover_de_url: row_album.cover_de_url,
        cid: row_album.cid,
        songs: Some(vec![]),
        artists: Some(vec![]),
        tags: Some(vec![]),
    };
    let relation_tags: Vec<&AlbumTag> = album_tag
        .iter()
        .filter(|relation| relation.album_cid == album.cid)
        .collect();
    if relation_tags.len() > 0 {
        let db_tags: Vec<&MTag> = tags
            .iter()
            .filter(|tag| {
                let relation = relation_tags
                    .iter()
                    .find(|relation| tag.cid == relation.tag_cid);
                if let Some(_) = relation {
                    return true;
                } else {
                    return false;
                }
            })
            .collect();
        album.tags = Some(
            db_tags
                .iter()
                .map(|db_tag| Tag {
                    cid: db_tag.cid,
                    tag: db_tag.tag.clone(),
                })
                .collect(),
        )
    }
    let relation_artists: Vec<&AlbumArtist> = album_artists
        .iter()
        .filter(|relation| relation.album_cid == album.cid)
        .collect();
    if relation_artists.len() > 0 {
        let db_tags: Vec<&MArtist> = artists
            .iter()
            .filter(|artist| {
                let relation = relation_artists
                    .iter()
                    .find(|relation| artist.cid == relation.artist_cid);
                if let Some(_) = relation {
                    return true;
                } else {
                    return false;
                }
            })
            .collect();
        album.artists = Some(
            db_tags
                .iter()
                .map(|db_artist| Artist {
                    cid: db_artist.cid,
                    name: db_artist.name.clone(),
                })
                .collect(),
        )
    }
    let mut rel_songs: Vec<Song> = vec![];

    for song in songs {
        if song.album_cid == album.cid {
            rel_songs.push(song.clone());
        }
    }
    album.songs = Some(rel_songs);
    album
}
