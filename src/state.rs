use std::sync::Arc;

use tokio::sync::RwLock;

use crate::{db::db_helper::DBHelper, schemas::schemas::Album};

pub struct GuardedAppState {
    pub state: AppState,
}

pub struct AppState {
    pub db: DBHelper,
    pub albums: Arc<RwLock<Vec<Album>>>,
}
